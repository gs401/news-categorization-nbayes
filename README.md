Approaching the text categorization problem with a Naive Bayes classifier

ESSENTIALS:
In order to run any of these scripts, you need to have Python (>=2.6 or >= 3.3) and the following packages:
NumPy (>= 1.6.1)
SciPy (>= 0.9)
scikit-learn (>= 0.16.1) - http://scikit-learn.org/stable/install.html

================================
RUNNING:
ON WINDOWS:
Open Command Prompt and navigate into the appropriate directory where the Python script is located. Example: if the script is in d:\text_categorization:
		> cd d:\text_categorization
Run the script `text_categorizer.py` in the following way:
		> python text_categorizer.py

ON UNIX-LIKE SYSTEMS (LINUX, MAC, ETC.):
Navigate to the directory where the Python script is located:
		$ cd ~/Downloads/text_categorization
Run the script `text_categorizer.py` in the following way:
		$ python text_categorizer.py

================================
PREDICTING CATEGORIES FOR RANDOM NEWS ARTICLE:
Create a text file (example:abc.txt) in the corresponding category folder in `dataset/bbc/mytest/`. Example: if I think a particular news article should be classified as `politics`, I will put the text file inside  `dataset/bbc/mytest/politcs/` folder. Then run:
		> python text_categorizer_mytest.py

================================