from sklearn.datasets import load_mlcomp
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import classification_report, confusion_matrix

# Fit the training data
training_data = load_mlcomp("bbc_datasets", "train", "dataset/")
vectorizer = CountVectorizer(encoding="latin1")
training_data_vectorized = vectorizer.fit_transform((open(f).read() for f in training_data.filenames))
training_data_labels = training_data.target

classifier = MultinomialNB().fit(training_data_vectorized,training_data_labels)

# Predict label for test article
test_data = load_mlcomp("bbc_datasets", "test", "dataset/")
test_data_vectorized = vectorizer.transform((open(f).read() for f in test_data.filenames))
prediction = classifier.predict(test_data_vectorized)
prediction_labels = [test_data.target_names[x] for x in prediction]

# Performance
print "Classification report on test set for classifier:"
print "Vectorizer: " + str(vectorizer)
print "Classifier: " + str(classifier)
print(classification_report(test_data.target, prediction,
                            target_names=test_data.target_names))

cm = confusion_matrix(test_data.target, prediction)
print "Confusion matrix:"
print cm
