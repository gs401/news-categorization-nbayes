from sklearn.datasets import load_mlcomp
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB

# Fit the training data
training_data = load_mlcomp("bbc_datasets", "train", "dataset/")
vectorizer = CountVectorizer(encoding="latin1")
training_data_vectorized = vectorizer.fit_transform((open(f).read() for f in training_data.filenames))
training_data_labels = training_data.target

classifier = MultinomialNB().fit(training_data_vectorized,training_data_labels)

# Predict label for test article
test_data = load_mlcomp("bbc_datasets", "mytest", "dataset/")
test_data_vectorized = vectorizer.transform((open(f).read() for f in test_data.filenames))
prediction = classifier.predict(test_data_vectorized)
prediction_labels = [test_data.target_names[x] for x in prediction]

i=0
while(i<len(prediction)):
    print("Filename: {}\n".format(test_data.filenames[i]))
    datapoint_snippet = "\"" + open(test_data.filenames[i]).read()[:100]+"...\""
    print("Content: {}\n".format(datapoint_snippet))
    print("Predicted label: {}".format(prediction_labels[i]))
    if(prediction_labels[i]==test_data.target_names[test_data.target[i]]):
        print("Prediction correct (treating directory name as true label)")
    else:
        print("Prediction false (treating directory name as true label)")
    print("---\n\n")
    i = i+1
